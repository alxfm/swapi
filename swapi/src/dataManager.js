import Vue from 'vue';
import { store } from './main';
import { moment } from './main';

export default {
  apiBase: 'https://swapi.co/api/',
  ifDataIsUpToDate () {
    const today = new Date();
    return moment(today).diff(moment(this.getLastUpdateTime()), 'hours') <  24;
  },
  async updateData () {
    Vue.localStorage.set('peopleData', null);
    Vue.localStorage.set('filmsData', null);

    let peopleData = [];
    let filmsData = [];

    const peopleRequest = new Promise(async (resolve, reject) => {
       let result = null;
       let i = 1;
       while (result === null || result.body.next !== null) {
         await Vue.http.get(this.apiBase + 'people/?page=' + i)
         .then(res => {
            result = res;
            peopleData = peopleData.concat(result.body.results);
            if (result.body.next === null)
              resolve (peopleData);
            else
              i++
          })
          .catch(err => {
            reject(err);
          });
    }});

    const filmsRequest = new Promise(async (resolve, reject) => {
      let result = null;
      let i = 1;
      while (result === null || result.body.next !== null) {
        await Vue.http.get(this.apiBase + 'films/?page=' + i)
          .then(res => {
            result = res;
            filmsData = filmsData.concat(result.body.results);
            if (result.body.next === null)
              resolve (peopleData);
            else
              i++
          })
          .catch(err => {
            reject(err);
          });
      }});

    await Promise.all([peopleRequest, filmsRequest]).then (
      res => {Vue.localStorage.set('dataUpdateTime', new Date().toString())},
      err => {
        alert(err);
        console.openFilm(err.body);
      }
      );

    for (let c = 0; c < peopleData.length; c++) {
      let character = peopleData[c];
      for (let f = 0; f < character.films.length; f++) {
        let c_film = character.films[f];
        for (let ff = 0; ff < filmsData.length; ff++) {
          let f_film = filmsData[ff];
          if (c_film === f_film.url) {
            peopleData[c].films[f] = {
              title: f_film.title,
              url: peopleData[c].films[f],
              id: peopleData[c].films[f].slice(27, peopleData[c].films[f].length - 1)
            };
          }
        }
      }
      peopleData[c].edited = this.humanizeDate(peopleData[c].edited);
    }

    if (peopleData !== [])
      Vue.localStorage.set('peopleData', JSON.stringify(peopleData));

    for (let f = 0; f < filmsData.length; f++) {
      let film = filmsData[f];
      for (let c = 0; c < film.characters.length; c++) {
        film.characters[c] = {
          name: this.getCharacterById(film.characters[c].slice(28, film.characters[c].length - 1)).name,
          id: film.characters[c].slice(28, film.characters[c].length - 1),
          url: film.characters[c]
        }
      }
      filmsData[f].edited = this.humanizeDate(filmsData[f].edited);
    }

    if (filmsData !== [])
      Vue.localStorage.set('filmsData', JSON.stringify(filmsData));
  },
  getCharacters() {
    return JSON.parse(Vue.localStorage.get('peopleData', '', JSON));
  },
  getCharacterById (id) {
    const characters = JSON.parse(Vue.localStorage.get('peopleData'));
    if (characters === null || characters === [])
      return [{}];

    return characters.find((element) => {
      return id === element.url.slice(28, element.url.length - 1);
    }) || [{}];
  },
  getCharacterByName (name) {
    const characters = JSON.parse(Vue.localStorage.get('peopleData'));
    if (characters === null || characters === [])
      return [{}];

    return characters.filter((element) => {
      return element.name.toLowerCase().match(name.toLowerCase());
    }) || [{}];
  },
  getFilms() {
    return JSON.parse(Vue.localStorage.get('filmsData', '', JSON));
  },
  getFilmById (id) {
    const films = JSON.parse(Vue.localStorage.get('filmsData'));
    if (films === null || films === [])
      return [{}];

    return films.find((element) => {
      return id === element.url.slice(27, element.url.length - 1);
    }) || [{}];
  },
  getFilmByName (name) {
    const films = JSON.parse(Vue.localStorage.get('filmsData'));
    if (films === null || films === [])
      return [{}];

    return films.filter((element) => {
      return element.title.toLowerCase().match(name.toLowerCase());
    })
  },
  getLastUpdateTime () {
    return Vue.localStorage.get('dataUpdateTime', '1970-01-01');
  },
  humanizeDate(date) {
    return moment(date).format('YYYY-MM-DD hh:mm:ss');
  }
}
