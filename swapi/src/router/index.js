import Vue from 'vue'
import Router from 'vue-router'
import DataView from '@/components/DataView'

Vue.use(Router)

import People from '@/components/People'
import PeopleRoot from '@/components/PeopleRoot'
import PeopleRoute from '@/components/PeopleRoute'

import Films from '@/components/Films'
import FilmsRoot from '@/components/FilmsRoot'
import FilmsRoute from '@/components/FilmsRoute'

export default new Router({
  routes: [
    {
      path: '/',
      component: DataView,
      children: [
        {
          path: 'people/',
          component: People,
          children: [
            {
              path: '',
              component: PeopleRoot,
            },
            {
              path: 'route/:id',
              component: PeopleRoute,
            },
          ]
        },
        {
          path: 'films/',
          component: Films,
          children: [
            {
              path: '',
              component: FilmsRoot,
            },
            {
              path: 'route/:id',
              component: FilmsRoute,
            },
          ]
        }
      ]
    }
  ]
})
