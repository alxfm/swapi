// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueResource from 'vue-resource';
import Vuex from 'vuex';
import VueLocalStorage from 'vue-localstorage';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';
import 'element-ui/lib/theme-chalk/index.css';
import router from './router';
import moment from 'moment';
import App from './App';

Vue.config.productionTip = false;

Vue.use(VueLocalStorage)
Vue.use(ElementUI, { locale });
Vue.use(VueResource);
Vue.use(Vuex);
Vue.prototype.moment = moment;

Vue.config.productionTip = false;

export { moment }

export const store = new Vuex.Store({
  state: {
    count: 3
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
